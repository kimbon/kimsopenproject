﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KimsOpenProject
{
    public class CalculatorImpl : ICalculator
    {
        public int Add(int a, int b)
        {
            return a + b;
        }

        public int Subtract(int a, int b)
        {
            return a - b;
        }
    }
}
