﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KimsOpenProject
{
    public interface ICalculator
    {
        int Add(int a, int b);

        int Subtract(int a, int b);
    }
}
